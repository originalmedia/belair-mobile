/* ==================== ---------- ====================*/
/* ==================== FORMATACAO ====================*/
/* ==================== ---------- ====================*/

var entidadeMarterdata  = "FC"; //sigla da entidade de dados criada no MASTERDATA (CRM)
var ambienteLoja        = "belairmoveis"; //endereco da loja onde será implantado o formulário
var senderForm          = new OmSendMessage(ambienteLoja,entidadeMarterdata);//Inicialização da LIB de envio de dados.

//acao do click do submit
function actionSubmit(){

    //define o assunto
    senderForm.setSubject($('#assunto').val());
    
    //define remetente
    senderForm.setSender($('[name="email"]').val());

    senderForm.setName($('[name="nome"]').val());

    //adiciona um titulo a mensagem (HTML livre)
    senderForm.messageAppend("<h1>Fale Conosco</h1>");
    
    // //adiciona um titulo a mensagem (HTML livre)
    // senderForm.messageAppend('<h2>mensagem gerada com o "autoMessage()"</h2>');
    
    //gera a mensagem automaticamente a partir dos atributos "name"
    //senderForm.autoMessage('contact',['nome','email','mensagem']);
    
    // //adiciona um titulo a mensagem (HTML livre)
    // senderForm.messageAppend('<h2>mensagem gerada com o "autoMessageByQuery()"</h2>');
    
    //gera a mensagem automaticamente a partir de uma seletor query
    senderForm.autoMessageByQuery($('#contact .mensagem'));
    
    //ENVIANDO OS DADOS ===========================================================================================
    var request = senderForm.sendMessageVTEX(ambienteLoja,entidadeMarterdata)//retorna uma request ajax (masterdata)
    //caso aja sucesso na requisicao
    request.done(function(data){
        $('#message').html($('<div>',{'class':'messageContent messageContentSuccess','text':'Dados enviados com sucesso'}).fadeIn())
        $('input, textarea').val('');
        senderForm = new OmSendMessage(ambienteLoja,entidadeMarterdata);//reset da LIB de envio de dados.
    });
    //caso aja erro na requisicao
    request.error(function(err){
        $('#message').html($('<div>',{'class':'messageContent messageContent messageContentError','text':'Erro ao enviar os dados'}).fadeIn())
    });
    
}   


$(function(){

    /* ==================== ---- ====================*/
    /* ==================== MASK ====================*/
    /* ========== jquery.maskedinput.min.js =========*/
    /* ==================== ---- ====================*/
    
    // $.mask.definitions['~'] = "[+-]";
    // $("#cpf").mask("999.999.999-99");
    // $("#telefone, #celular").mask("(99) 9999-9999?9");

    
    /* ==================== ------- ====================*/
    /* ==================== ACTIONS ====================*/
    /* ==================== ------- ====================*/

    //validando os campos
    $( 'input[type="text"][validate], select[validate], textarea[validate],input[type="date"][validate]' ).focusout(function(){
        ValidateField( $(this) );
    });

    //validando o campo de email
    $( 'input[name=email]' ).focusout(function(){
        if(validEmail($(this).val())){
            $(this).removeClass('error-input')
            $('#message').html('')            
        }else{
            $(this).addClass('error-input')
            $('#message').html($('<div>',{'class':'messageContent messageContentError','text':'Email inválido'}).fadeIn())            
        }

    })

    // Chama a função de validacao do formulario no submit do formulario
    $( 'input[name="BTEnvia"]' ).click(function(e){

        e.preventDefault;
        if (validateForm()) {
            actionSubmit() //apos validado o formulario a mensagem e enviada

        }else{
            $('#message').html($('<div>',{'class':'messageContent messageContentError','text':'* Os campos com asterisco são obrigatórios'}).fadeIn())
        }
         
    });

    if (window.File && window.FileReader && window.FileList && window.Blob) {
        $( 'input[type="file"]' ).on('change', function(evt) {
            var files = evt.target.files;
            var file = files[0];

            if (files && file) {
                var reader = new FileReader();

                reader.onload = function(readerEvt) {
                    var binaryString = readerEvt.target.result;
                    senderForm.setFile(btoa(binaryString));
                };

                reader.readAsBinaryString(file);
            }
        }, false);
    }
});

