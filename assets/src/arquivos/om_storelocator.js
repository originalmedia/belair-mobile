/*
  Original Media
  author: Zeonardo Lima
  Vanilla Store Locator on Google Maps
  Version: 1.0.0
*/
window.onload = function() {
  
    window.om_StoreLocator = {};
    om_StoreLocator.directionsService = new google.maps.DirectionsService();
    om_StoreLocator.map = new google.maps.Map(document.getElementById('hidden_map'));
    om_StoreLocator.accordionHeaders = document.querySelectorAll('.contentStores .store h2');
    om_StoreLocator.maxResults = 3;
    om_StoreLocator.stores = [];
    om_StoreLocator.markers = [];

    window.myloc = document.getElementById("myloc");
    var searchLoc = document.getElementById('search-loc');
    var autocomplete = new google.maps.places.Autocomplete(searchLoc);

    function getMyLoc(){
      var loc = {
        lat: 0,
        lng: 0
      };

      if(myloc && myloc.value){
        var ml = myloc.value.split(',');
        loc.lat = parseFloat(ml[0]);
        loc.lng = parseFloat(ml[1]);
      }
      
      return loc;
    }

    function loadStores(){

      var domStores = document.querySelectorAll('.contentStores .store');
      for (var i = 0; i < domStores.length; i++) {
        var store = domStores[i];
        if(store.dataset.lat && store.dataset.lng){
          store.id = 'store_' + i;
          var m = document.createElement('div');
          m.id = 'map_' + i;
          m.classList.add('gmap');
          store.appendChild(m);
          var title = store.querySelector('h2').innerText;
          var s = {
            index: i,
            title: title,
            pos: {
              lat: parseFloat(store.dataset.lat),
              lng: parseFloat(store.dataset.lng)
            }
          };
          om_StoreLocator.stores.push(s);
        }
      }

      loadMainMap();
    }

    function loadMainMap(){
      if (om_StoreLocator.stores.length == 0) {
          return;
      }

      // Clear out the old markers.
      om_StoreLocator.markers.forEach(function(marker) {
          marker.setMap(null);
      });
      om_StoreLocator.markers = [];

      // For each place, get the icon, name and location.
      var bounds = new google.maps.LatLngBounds();
      om_StoreLocator.stores.forEach(function(store) {
          
          // Create a marker for each place.
          om_StoreLocator.markers.push(new google.maps.Marker({
              //icon: icon,
              title: store.title,
              position: store.pos,
              map: om_StoreLocator.map,
          }));

          bounds.extend(store.pos);
      });
      om_StoreLocator.map.fitBounds(bounds);
    }

    function loadStoreMap(storeMap, store){

      var smap = new google.maps.Map(storeMap, {
          center: store.pos,
          zoom: 14,
          mapTypeId: 'roadmap'
      });

      var marker = new google.maps.Marker({
        title: store.title,
        position: store.pos,
        map: smap
      });

      if(storeMap.classList.contains('loc-done')){
        return;
      }

      var storeContainer = document.getElementById('store_'+store.index);

      om_StoreLocator.accordionHeaders[store.index].onclick = function(e){
        e.preventDefault();
        for (var i = 0; i < om_StoreLocator.accordionHeaders.length; i++) {
          i != store.index && om_StoreLocator.accordionHeaders[i].parentNode.classList.remove('active');
        }
        e.target.parentNode.classList.toggle('active');
        return false;
      };
      
      //clear[i].classList.remove('loc-selected');

      var actions = document.createElement('div');
      actions.classList.add('store-actions');

      var phoneLink = document.createElement('a');
      phoneLink.href = "#";
      phoneLink.classList.add('store-phone');
      phoneLink.text = "Ligar";
      actions.appendChild(phoneLink);

      var phoneList = document.createElement('div');
      phoneList.classList.add('store-phone-list');

      var phones = (storeMap.parentNode.textContent || '').match(/\(?(\d{2})\)?[- ]?(\d{4})[- ]?(\d{4})/g);
      if(phones && phones.length){
        if(phones.length > 1){
          phoneLink.onclick = function(e){
            e.preventDefault();
            e.target.classList.toggle('active');
            phoneList.classList.toggle('active');
            return false;
          }

          for (var i = 0; i < phones.length; i++) {
            var p = document.createElement('a');
            p.href = "tel:"+phones[i];
            p.classList.add('phone-list-item');
            p.text = phones[i];
            phoneList.appendChild(p);
          }
        }
        else{
          phoneLink.href = "tel:"+phones[0];
        }
      }

      var directionLink = document.createElement('a');
      directionLink.href = "#";
      directionLink.classList.add('store-direction');
      directionLink.text = "Traçar Rota";
      directionLink.onclick = function(e){
        e.preventDefault();
        getDirections(smap, marker);
        return false;
      }
      actions.appendChild(directionLink);

      actions.appendChild(phoneList);
      storeContainer.insertBefore(actions, storeMap);
      storeMap.classList.add('loc-done');
    }

    function getDirections(storeMap, marker){
      marker.setMap(null);

      var directionsDisplay = new google.maps.DirectionsRenderer();
      directionsDisplay.setMap(storeMap);

      var request = {
          origin: getMyLoc(),
          destination: marker.position,
          travelMode: 'DRIVING',
          unitSystem: google.maps.UnitSystem.METRIC
      };

      om_StoreLocator.directionsService.route(request, function(response, status) {
          if (status == 'OK') {
              directionsDisplay.setDirections(response);
          }
          else{

          }
      });
    }

    function rad(x) {return x*Math.PI/180;}
    function findClosestStores(pos) {
        pos = pos || getMyLoc();
        var lat = pos.lat;
        var lng = pos.lng;
        var R = 6371; // radius of earth in km
        var distances = [];
        var closest = -1;
        for(var i=0;i< om_StoreLocator.markers.length; i++ ) {
            var mlat = om_StoreLocator.markers[i].position.lat();
            var mlng = om_StoreLocator.markers[i].position.lng();
            var dLat  = rad(mlat - lat);
            var dLong = rad(mlng - lng);
            var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(rad(lat)) * Math.cos(rad(lat)) * Math.sin(dLong/2) * Math.sin(dLong/2);
            var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
            var d = R * c;
            distances[i] = {
              index: i,
              name: om_StoreLocator.markers[i].title,
              distance: d
            };
            if ( closest == -1 || d < distances[closest].distance ) {
                closest = i;
            }
        }

        distances = distances.sort(function(a, b){
          return a.distance - b.distance;
        }).slice(0, om_StoreLocator.maxResults);

        clearStores();
        for (var j = 0; j < distances.length; j++) {
          showStore(distances[j].index);
        }

        sideSlide(distances.length);
        return distances;
    }

    function clearStores(){
      var clear = document.querySelectorAll('.contentStores .loc-selected');
      for (var i = 0; i < clear.length; i++) {
        clear[i].classList.remove('loc-selected');
        clear[i].classList.remove('active');
      }
    }

    function showStore(index){

      document.getElementById('store_'+index).classList.add('loc-selected');
      loadStoreMap(document.getElementById('map_'+index), om_StoreLocator.stores[index]);
    }

    //TODO: Vanillate it!
    function sideSlide(resultsCount){
      if(typeof $ == 'undefined'){
        return;
      }

      var parent = $('.box-lojas');
      var scrollTo = $('.contentStores');
      parent.addClass('scrollable scrolled').animate({ scrollLeft: scrollTo.offset().left }, 600);

      var resultMessage = resultsCount > 1 ? ('Foram encontradas '+ '<em>'+resultsCount+'</em> Lojas') : ('Foi encontrada '+'<em>'+resultsCount+'</em> Loja');
      resultMessage += ' perto da sua localização.'
      if(!$('.contentStores .loc-unscroll').length){
        $('.contentStores').prepend($('<a href="#" class="loc-unscroll"><i class="icon-arrow"></i><span></span></a>'));
        $('.contentStores .loc-unscroll').click(function(e){
          e.preventDefault();
          parent.removeClass('scrolled').animate({ scrollLeft: 0 }, 300);
          return false;
        });
      }
      $('.contentStores .loc-unscroll span').html(resultMessage);
    }

    document.getElementById('get-loc').onclick = function(e){
        e.preventDefault();
        document.getElementById('getloc-error').innerText = '';
        if (navigator.geolocation) {
          function success(pos){
            e.target.classList.remove('no-loc');
            myloc.value = pos.coords.latitude+','+pos.coords.longitude;

            findClosestStores({
              lat: pos.coords.latitude,
              lng: pos.coords.longitude
            });
          }
          function error(err){
            e.target.classList.add('no-loc');
            document.getElementById('getloc-error').innerText = 'Erro ao obter localização:\n'+err.message;
            console.warn(`ERROR(${err.code}): ${err.message}`);
          }
          navigator.geolocation.getCurrentPosition(success, error, {
            enableHighAccuracy: true,
            timeout: 5000,
            maximumAge: 0
          });
        } else {
            e.target.classList.add('no-loc');
            document.getElementById('getloc-error').innerText = 'Não foi possível obter sua localização';
        }
        return false;
    }

    document.getElementById('loc-form').onsubmit = function(e){
      e.preventDefault();
      findClosestStores();
      return false;
    };

    
    google.maps.event.addListener(autocomplete, 'place_changed', function() {
      var place = autocomplete.getPlace();
      document.getElementById('searchloc-error').innerText = '';
      if(place && place.geometry){
        myloc.value = place.geometry.location.lat()+','+place.geometry.location.lng();
      }
      else{
        document.getElementById('searchloc-error').innerText = 'Não foi possível encontrar o endereço buscado';
      }
    });

    loadStores();
}
